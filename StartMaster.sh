#!/bin/bash -x
#SBATCH --job-name=spark-master
#SBATCH -n 40
#SBATCH --time=99:99:99          # total run time limit (HH:MM:SS)
#SBATCH --output=Output.o
#SBATCH --error=Last.e
#SBATCH -w c1

# Default port of spark is 7077
# You can tunnel to master to see jobs in browser


# rclone config
service=cloud
remoteOut=hpc/output

# Ambiente config
localJob="/scratch/job.${SLURM_JOB_ID}"
localIn="${localJob}/input/"
localOut="${localJob}/output/"

# Spark config
masterNode=c1
sparkSimg="Spark.sif"
sparkLogPath="${localJob}/log"

function cleanJob(){
  echo "Limpando ambiente..."
  rm -rf "${localJob}"
}
trap cleanJob EXIT HUP INT TERM ERR

mkdir -p "${sparkLogPath}"

echo "Starting master..."
srun singularity exec \
  --bind=/scratch:/scratch \
  --bind=/var/spool/slurm:/var/spool/slurm \
  --bind=/scratch/:/opt/apache-spark/logs \
  $sparkSimg run-master.sh -h $masterNode -p 7077


# Maybe add the submit in this script to?
# jarApp="PATH/TO/COMPILED/JAR"
# Submit the job with:
# spark-submit --master $masterNode --total-executor-cores ${TEC} --executor-memory ${EM}G $jarApp


